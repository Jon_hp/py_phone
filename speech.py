#!/data/data/com.termux/files/usr/bin/python3
import json
import os

def say(words):
    string="termux-tts-speak \"{}\"".format(words)
    os.system(string)
def listen():
    os.system('termux-dialog speech>/data/data/com.termux/files/home/phone/db/spoken.json')
    batfile=open("/data/data/com.termux/files/home/phone/db/spoken.json","r")
    heard=json.load(batfile)
    batfile.close()
    return heard["text"]
