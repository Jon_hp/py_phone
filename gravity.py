#!/data/data/com.termux/files/usr/bin/python3
#gravity.py
import json
import os

#return an array of three items (x,y,z)
def get():
    os.system('termux-sensor -s gravity -n 1 > /data/data/com.termux/files/home/phone/db/gravity.json')
    g_file=open("/data/data/com.termux/files/home/phone/db/gravity.json","r")
    gravity=json.load(g_file)
    g_file.close()
    return gravity["Gravity"]["values"]
